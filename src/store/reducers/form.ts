import ReactFormAction from "../actions/form";
import {ReactFormsActionTypes} from "../actionsTypes";
import {Reducer} from "redux";

export interface DefaultFormState {
    [form: string]: {
        [field: string]: any
    }
}

const defaultState: DefaultFormState = {

};

const formReducer: Reducer<DefaultFormState> = (state: DefaultFormState = defaultState, action: ReactFormAction) => {
    switch (action.type) {
        case ReactFormsActionTypes.CREATE_FORM:
            return {
                ...state,
                [action.name]: {}
            };
        case ReactFormsActionTypes.DESTROY_FORM:
            let newState = {...state};
            delete newState[action.name];
            return newState;
        case ReactFormsActionTypes.UPDATE_FORM_FIELD:
            return {
                ...state,
                [action.name]: {
                    ...state[action.name],
                    [action.key]: action.value
                },
            };
        default:
            return state;
    }
};

export default formReducer;