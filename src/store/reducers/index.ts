import {combineReducers, Reducer} from "redux";
import formReducer, {DefaultFormState} from './form';

export interface RootState {
    forms: DefaultFormState,
}

const rootReducer: Reducer<RootState> = combineReducers({
    forms: formReducer,
});

export default rootReducer;