export const ReactFormsActionTypes = {
    CREATE_FORM: '@@react-forms/Create form',
    DESTROY_FORM: '@@react-forms/Destroy form',
    UPDATE_FORM_FIELD: '@@react-forms/Update form field',
};

