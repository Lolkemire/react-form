import {Store} from "react-redux";
import {default as rootReducer, RootState} from "./reducers/index";
import {createStore} from "redux";
import {composeWithDevTools} from "redux-devtools-extension";


const store: Store<RootState> = createStore(rootReducer, composeWithDevTools());

export default store;