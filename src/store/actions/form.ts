import {Action} from "redux";
import {ReactFormsActionTypes} from "../actionsTypes";


export class CreateFormAction implements Action {
    type: string = ReactFormsActionTypes.CREATE_FORM;

    constructor(public name: string) {

    }
}

export class DestroyFormAction implements Action {
    type: string = ReactFormsActionTypes.DESTROY_FORM;

    constructor(public name: string) {

    }
}

export class UpdateFieldAction implements Action {
    type: string = ReactFormsActionTypes.UPDATE_FORM_FIELD;

    constructor(public name: string, public key: string, public value: any) {

    }
}

type ReactFormAction = CreateFormAction & DestroyFormAction & UpdateFieldAction;
export default ReactFormAction;