import * as React from "react";
import {ChangeEvent} from "react";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {DefaultFormState} from "../store/reducers/form";
import {ReactFormContextTypes} from "./ReactForm";


export interface ReactFieldStateProps {
    getValue: (formName: string, fieldName: string) => any,
}

export interface ReactFieldProps  {
    fieldName: string,
    value?: any;
};

const mapStateToProps = (state: DefaultFormState): ReactFieldStateProps => {
    return {
        getValue: (formName: string, fieldName: string) => {
            const form = state[formName];
            return form ? form[fieldName] : undefined;
        }
    };
};

export const reactFormField = (WrapComponent: React.ComponentType) => {
    class Field extends React.Component<any> {
        static contextTypes = {
            onFieldChange: PropTypes.func,
            formName: PropTypes.string,
        };

        context: ReactFormContextTypes;

        onChange = (event: ChangeEvent<any>) => {
            event.preventDefault();
            const name = event.target.name;
            const value = event.target.value;
            this.context.onFieldChange(name, value);
        };

        getValue = () => {
            const form = this.context.formName;
            const field = this.props.fieldName;
            return this.props.getValue(form, field);
        };

        componentDidMount() {
        }

        render() {
            return (
                <WrapComponent {...this.props}/>
            )
        }
    }
    return connect(mapStateToProps)(Field)
};
