import * as React from "react";
import {RootState} from "../store/reducers/index";
import {connect, Dispatch} from "react-redux";
import ReactFormAction from "../store/actions/form";
import {ReactFormsActionTypes} from "../store/actionsTypes";
import PropTypes from "prop-types";

export interface ReactFormConfig {
    name: string;
}

export interface ReactFormStateProps {
}

export interface ReactFormActionsProps {
    initForm: (name: string) => void;
    onFieldChange: (formName: string, key: string, value: any) => void;
}

export interface ReactFormContextTypes {
    onFieldChange: (key: string, value: any) => void;
    formName: string,
}

const mapStateToProps = (state: RootState): ReactFormStateProps => {
    return {

    }
};

const mapDispatchToProps = (dispatch: Dispatch<ReactFormAction>): ReactFormActionsProps => {
    return {
        initForm: (name: string) => {
            // dispatch(new CreateFormAction(name));
            dispatch({
                type: ReactFormsActionTypes.CREATE_FORM,
                name,
            });
        },
        onFieldChange: (formName: string, key: string, value: any) => {
            dispatch({
                type: ReactFormsActionTypes.UPDATE_FORM_FIELD,
                name: formName,
                key,
                value,
            });
        }

    }
};

type ReactFormProps = ReactFormStateProps & ReactFormActionsProps;


export const reactForm = (config: ReactFormConfig) => {
    return (WrapComponent: React.ComponentType<any>) => {
        type propTypes = typeof WrapComponent.propTypes;
        type WrappedFormProps = propTypes & ReactFormProps;

        class WrappedForm extends React.Component<WrappedFormProps, {}> implements
            React.ChildContextProvider<ReactFormContextTypes> {

            static childContextTypes = {
                onFieldChange: PropTypes.func,
                formName: PropTypes.string,
            };

            getChildContext(): ReactFormContextTypes {
                return {
                    onFieldChange: this.onFieldChange,
                    formName: config.name,
                };
            }

            onFieldChange = (key: string, value: any) => {
                this.props.onFieldChange(config.name, key, value);
            };

            componentWillMount() {
                this.props.initForm(config.name);
            };

            render() {
                return (
                    <WrapComponent {...this.props}/>
                );
            }
        }

        return connect(mapStateToProps, mapDispatchToProps)(WrappedForm);
    };
};