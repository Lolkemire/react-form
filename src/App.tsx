import WidthTest from './components/WidthTest/WidthTest';
import {default as React} from 'react';
import './App.css';
import LoginForm from "./components/LoginForm";

const logo = require('./logo.svg');

class App extends React.Component {
    state = {
        widthChildren: [
            <div style={{width: "200px"}}>Some text</div>,
            <div style={{width: "300px"}}>SOME LONG TEXT</div>
        ],
    }

    addChild() {
        const maxWidth = Math.floor(Math.random() * 3000);
        const children = [...this.state.widthChildren];
        children.push(
        <div style={{width: `${maxWidth}px`}}>SOME LONG TEXT (width {maxWidth})</div>);
        this.setState(prevState => (
            {
                widthChildren: children
            }
        ))
    }

    render() {
        return (
            <div className="App">
                <div className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h2>Welcome to React</h2>
                </div>
                <LoginForm/>
                <button onClick={this.addChild.bind(this)}>KEK</button>
                <WidthTest>
                    {this.state.widthChildren}
                </WidthTest>
            </div>
        );
    }
}

export default App;
