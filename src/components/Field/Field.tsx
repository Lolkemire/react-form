import * as React from "react";
import {ChangeEvent} from "react";
import {DefaultFormState} from "../../store/reducers/form";
import {connect} from "react-redux";
import {ReactFormContextTypes} from "../../hocs/ReactForm";
import PropTypes from "prop-types";




export interface ReactFieldStateProps {
    getValue: (formName: string, fieldName: string) => any,
}

export type ReactFieldProps = ReactFieldStateProps & {
    fieldName: string,
    component?: string | React.ComponentType<any>,
    type?: string;
};

const mapStateToProps = (state: DefaultFormState): ReactFieldStateProps => {
    return {
        getValue: (formName: string, fieldName: string) => {
            const form = state[formName];
            return form ? form[fieldName] : undefined;
        }
    };
};


class Field extends React.Component<ReactFieldProps> {
    static contextTypes = {
        onFieldChange: PropTypes.func,
        formName: PropTypes.string,
    };

    static defaultProps = {
        type: "text",
        component: "input",
    };

    context: ReactFormContextTypes;

    onChange = (event: ChangeEvent<any>): void => {
        event.preventDefault();
        const name = event.target.name;
        const value = event.target.value;
        this.context.onFieldChange(name, value);
    };

    getValue = () => {
        const form = this.context.formName;
        const field = this.props.fieldName;
        return this.props.getValue(form, field);
    };

    componentDidMount() {
    }

    render() {
        const Component: any = this.props.component;
        return (
            <Component name={this.props.fieldName} type={this.props.type}
                       onChange={this.onChange}
                       value={this.getValue()}/>
        )
    }
}

export default connect(mapStateToProps)(Field);