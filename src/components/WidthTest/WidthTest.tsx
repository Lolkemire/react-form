import * as React from "react";
import * as ReactDOM from "react-dom";

interface WidthTestState {
    maxWidth: number;
    children: any[]
}

export default class WidthTest extends React.Component<any, WidthTestState> {
    state: WidthTestState = {
        maxWidth: 0,
        children: this.props.children
    }
    childrenRefs: any = {}

    handleChildren() {
        return React.Children.map(this.props.children, (child: React.ReactChild, index: number) => {
            const clonedChild = React.cloneElement(child as any, {
                ref: (obj: any) => this.childrenRefs['child-'+index] = obj
            });
            return clonedChild;
        })
    }

    calculateMaxWidth() {
        React.Children.forEach(this.props.children, (child: any, index: number) => {
            const node = ReactDOM.findDOMNode(this.childrenRefs[`child-${index}`])
            if (node.clientWidth > this.state.maxWidth) {
                this.setState(prevState => (
                    {
                        maxWidth: node.clientWidth,
                    }
                ))
            }
        });
    }

    componentDidMount() {
        this.calculateMaxWidth();
    }

    componentDidUpdate() {
        this.calculateMaxWidth();
    }


    render() {
        return (
            <div>
                <span>{this.state.maxWidth}</span>
                {this.handleChildren()}
            </div>
        )
    }
}