import * as React from "react";
import Field from "./Field/Field";


interface MyInputProps {
    name: string,
    type: string
}


export default class MyInput extends React.Component<MyInputProps, {}> {
    render() {
        return <Field fieldName={this.props.name}
                      type={this.props.type}
                      component="input"/>
    }
}