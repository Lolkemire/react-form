import * as React from "react";
import {reactForm} from "../hocs/ReactForm";
import Field from "./Field/Field";
import MyInput from "./Input";

class LoginForm extends React.Component<any, {}> {
    render() {
        return (
            <form>
                <div>
                    <label htmlFor="login">Login</label>
                    <Field fieldName="login" type="text"/>
                </div>
                <div>
                    <MyInput name="password" type="password"/>
                </div>
            </form>
        )
    }
}

export default reactForm({
    name: 'login-form',
})(LoginForm);